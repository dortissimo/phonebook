import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import {Contact} from '../../model/contact';

@Component({
  selector: 'app-contact-option',
  templateUrl: './contact-option.component.html',
  styleUrls: ['./contact-option.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContactOptionComponent implements OnInit, OnChanges {

  @Input() contact: Contact;
  @Output() selectContact: EventEmitter<string> = new EventEmitter();

  constructor(private cd: ChangeDetectorRef) { }

  ngOnInit() {
  }

  select() {
    this.selectContact.emit(this.contact.id);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.cd.markForCheck();
  }

}
