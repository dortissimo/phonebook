import { Pipe, PipeTransform } from '@angular/core';
import {Contact} from '../model/contact';

@Pipe({
  name: 'fullName'
})
export class FullNamePipe implements PipeTransform {

  transform(value: Contact, args?: any): any {
    return value.firstName + ' ' + value.lastName;
  }

}
