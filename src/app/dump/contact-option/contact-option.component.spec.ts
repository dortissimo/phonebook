import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactOptionComponent } from './contact-option.component';

describe('ContactOptionComponent', () => {
  let component: ContactOptionComponent;
  let fixture: ComponentFixture<ContactOptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactOptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactOptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
