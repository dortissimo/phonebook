import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Contact} from '../../model/contact';
import {Router} from '@angular/router';
import {ContactService} from '../../services/contact.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-all-contacts',
  templateUrl: './all-contacts.component.html',
  styleUrls: ['./all-contacts.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AllContactsComponent implements OnInit, OnDestroy {

  public contacts$: Observable<any>;

  constructor(private router: Router,
              private contactService: ContactService) { }

  ngOnInit() {
    this.contacts$ = this.contactService.getAllFilteredContacts();
  }

  select(event: Contact) {
    this.router.navigate(['edit', event]);
  }

  filter(str: string) {
    this.contactService.filter(str);
  }

  ngOnDestroy(): void {
  }

}
