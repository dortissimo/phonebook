import {PhoneNumber} from './phoneNumber';

export interface Contact {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  birthday: string;
  phoneNumbers: PhoneNumber[];
  avatarUrl: string;
}
