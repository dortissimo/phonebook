import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Contact} from '../../model/contact';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContactListComponent implements OnInit {
  @Input() contacts$: Observable<any>;

  @Output() selectContact: EventEmitter<Contact> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  select(event) {
    this.selectContact.emit(event);
  }

}
