import {ChangeDetectionStrategy, Component, OnDestroy} from '@angular/core';
import {ContactService} from './services/contact.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnDestroy {
  title = 'phonebook';

  constructor(private contactService: ContactService) { }

  ngOnDestroy(): void {
    this.contactService.unsubscribeForAllContacts();
  }
}
