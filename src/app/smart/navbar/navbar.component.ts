import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ContactService} from '../../services/contact.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent implements OnInit {

  birthdayContacts$: Observable<any>;

  constructor(private contactService: ContactService) { }

  ngOnInit() {
    this.birthdayContacts$ = this.contactService.filterTodaysBirthdays();
  }

}
