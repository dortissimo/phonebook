import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import {Contact} from '../model/contact';
import DocumentReference = firebase.firestore.DocumentReference;
import QuerySnapshot = firebase.firestore.QuerySnapshot;
import QueryDocumentSnapshot = firebase.firestore.QueryDocumentSnapshot;
import Firestore = firebase.firestore.Firestore;
import {BehaviorSubject, combineLatest, Observable, Subject} from 'rxjs';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  private db: Firestore;

  private contacts$: BehaviorSubject<Contact[]> = new BehaviorSubject([]);
  private contactsFilter$: BehaviorSubject<string> = new BehaviorSubject('');

  constructor() {
    firebase.initializeApp({
      apiKey: 'AIzaSyClJLHplRvGVX5Q5OzV0UYfi44R8SqfjG0',
      projectId: 'phonebook-1db9d',
    });

    this.db = firebase.firestore();

    this.subscribeForAllContactsRealtime();
  }

  createContact(contact: Contact): Promise<DocumentReference> {
    return this.db.collection('contacts').add(contact);
  }

  getAllFilteredContacts(): Observable<any> {
    return combineLatest(this.contacts$, this.contactsFilter$).pipe(
      map(([contacts, contactFilter]) => {
        return contacts.filter(contact => {
          // Filter by firstName, lastName and phoneNumbers
          return  contact.firstName.toLowerCase().includes(contactFilter.toLowerCase()) ||
            contact.lastName.toLowerCase().includes(contactFilter.toLowerCase()) ||
            contact.phoneNumbers.filter(phoneNumber => {
              const phoneNumberAsNumbersString = phoneNumber.phoneNumber.match(/\d+/g);
              const contactFilterAsNumbersString = contactFilter.match(/\d+/g);
              if (phoneNumberAsNumbersString && contactFilterAsNumbersString) {
                return phoneNumberAsNumbersString.join('').includes(contactFilterAsNumbersString.join(''));
              }
              return false;
            }).length > 0;
        });
      }),
    );
  }

  filter(str: string) {
    this.contactsFilter$.next(str);
  }

  subscribeForAllContactsRealtime() {
    this.db.collection('contacts').onSnapshot((querySnapshot: QuerySnapshot) => {
      const tmpContacts: Contact[] = [];
      querySnapshot.forEach((doc: QueryDocumentSnapshot) => {
        const tmpContact = doc.data() as Contact;
        tmpContact.id = doc.id;
        tmpContacts.push(tmpContact);
      });
      this.contacts$.next(tmpContacts);
      this.filterTodaysBirthdays();
    });
  }

  unsubscribeForAllContacts() {
    this.contacts$.unsubscribe();
    this.contactsFilter$.unsubscribe();
  }

  getContactById(id: string) {
    return this.db.collection('contacts').doc(id);
  }

  updateContactById(id: string, updatedContact: Contact) {
    this.db.collection('contacts').doc(id).set(updatedContact).catch(reason => console.error(reason));
  }

  removeContactById(id: string) {
    return this.db.collection('contacts').doc(id).delete().catch(reason => console.error(reason));
  }

  filterTodaysBirthdays() {
    const today = new Date();
    return this.contacts$.asObservable().pipe(
      map(contacts => {
        return contacts.filter(contact => {
          const contactsBirthday = new Date(contact.birthday);
          return (today.getDate() === contactsBirthday.getDate()) && ((today.getMonth() === contactsBirthday.getMonth()));
        });
      })
    );
  }
}
