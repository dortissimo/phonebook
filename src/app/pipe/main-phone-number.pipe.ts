import { Pipe, PipeTransform } from '@angular/core';
import {Contact} from '../model/contact';

@Pipe({
  name: 'mainPhoneNumber'
})
export class MainPhoneNumberPipe implements PipeTransform {

  transform(value: Contact, args?: any): any {
    const phoneNumber = value.phoneNumbers.find(phoneNum => phoneNum.isMain);
    return phoneNumber !== undefined ? phoneNumber.phoneNumber : 'Phone number not set';
  }

}
