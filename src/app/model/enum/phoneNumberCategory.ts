export enum PhoneNumberCategory {

  MAIN = 'main',
  HOME = 'home',
  WORK = 'work',
  OTHER = 'other'
}
