import {ChangeDetectionStrategy, Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-contact-search',
  templateUrl: './contact-search.component.html',
  styleUrls: ['./contact-search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContactSearchComponent implements OnInit, OnDestroy {

  filter: FormControl;
  @Output() filterString: EventEmitter<string> = new EventEmitter();
  private destroy$: Subject<boolean> = new Subject<boolean>();

  constructor() { }

  ngOnInit() {
    this.initFilter();
    this.createFilterStream();
  }

  initFilter() {
    this.filter = new FormControl('');
  }

  createFilterStream() {
    this.filter.valueChanges.pipe(
      takeUntil(this.destroy$)
    ).subscribe(value => this.filterString.emit(value));
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
