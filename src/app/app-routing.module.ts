import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AllContactsComponent} from './smart/all-contacts/all-contacts.component';
import {CreateEditContactComponent} from './smart/create-edit-contact/create-edit-contact.component';

const routes: Routes = [
  {
    path: '',
    component: AllContactsComponent
  },
  {
    path: 'create',
    component: CreateEditContactComponent,
    data: {
      editMode: false
    }
  },
  {
    path: 'edit/:id',
    component: CreateEditContactComponent,
    data: {
      editMode: true
    }
  },
  {
    path: '**',
    redirectTo: '/'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
