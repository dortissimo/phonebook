import {Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {PhoneNumberCategory} from '../../model/enum/phoneNumberCategory';
import {PhoneNumber} from '../../model/phoneNumber';
import {Contact} from '../../model/contact';
import {ActivatedRoute, Router} from '@angular/router';
import {ContactService} from '../../services/contact.service';

declare  var $: any;

@Component({
  selector: 'app-create-edit-contact',
  templateUrl: './create-edit-contact.component.html',
  styleUrls: ['./create-edit-contact.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateEditContactComponent implements OnInit {

  editableContact: Contact;
  editableContactId: string;
  public editMode: boolean;
  contactForm: FormGroup;
  phoneNumberFormArray: FormArray;
  phoneNumberCategory = PhoneNumberCategory;

  constructor(private fb: FormBuilder,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private contactService: ContactService,
              private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.initForm();
    this.initDatepicker();
    this.editMode = this.activatedRoute.snapshot.data.editMode;
    if (this.editMode) {
      this.activatedRoute.params.subscribe(params => {
        this.editableContactId = params.id;
        this.getEditableContact(this.editableContactId);
      });
    } else {
      this.addPhoneNumberGroup();
    }
  }

  initForm() {
    this.contactForm = this.fb.group({
      firstName: new FormControl('', [Validators.required, Validators.pattern('^[A-Za-z-& ‘`.\u0027]{1,25}$')]),
      lastName: new FormControl('', [Validators.required, Validators.pattern('^[A-Za-z-& ‘`.\u0027]{1,25}$')]),
      email: new FormControl('', Validators.email),
      birthday: new FormControl(''),
      phoneNumbers: this.fb.array([])
    });
    this.phoneNumberFormArray = this.contactForm.get('phoneNumbers') as FormArray;
  }

  initDatepicker() {
    const birthdayDatepicker = $('#birthdayInput');
    birthdayDatepicker.datepicker({
      format: 'mm/dd/yyyy'
    }).on('changeDate', (e) => {
      this.contactForm.controls.birthday.setValue(e.date.toLocaleDateString());
      this.contactForm.markAsTouched();
      this.cd.markForCheck();
    });
  }

  createPhoneNumberGroup(phoneNumber?: PhoneNumber): FormGroup {
    const phoneNumberGroup = this.fb.group({
      phoneNumber: new FormControl('', [Validators.required, Validators.pattern(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/)]),
      isMain: new FormControl(false),
      category: new FormControl(PhoneNumberCategory.MAIN)
    });
    if (!!phoneNumber) {
      phoneNumberGroup.get('phoneNumber').setValue(phoneNumber.phoneNumber);
      phoneNumberGroup.get('isMain').setValue(phoneNumber.isMain);
      phoneNumberGroup.get('category').setValue(phoneNumber.category);
    } else if (this.phoneNumberFormArray.length === 0) {
      phoneNumberGroup.get('isMain').setValue(true);
    }
    return phoneNumberGroup;
  }

  addPhoneNumberGroup(phoneNumber?: PhoneNumber): void {
    if (!!phoneNumber) {
      this.phoneNumberFormArray.push(this.createPhoneNumberGroup(phoneNumber));
    } else {
      this.phoneNumberFormArray.push(this.createPhoneNumberGroup());
    }
    this.cd.markForCheck();
  }

  removePhoneNumberGroup(index: number) {
    this.phoneNumberFormArray.removeAt(this.phoneNumberFormArray.controls.findIndex((group, i) => i !== index));
    this.checkIsMainControl();
    this.contactForm.markAsTouched();
    this.cd.markForCheck();
  }

  checkIsMainControl() {
    if (this.phoneNumberFormArray.controls.length > 0) {
      const isMainPhoneNumberCount = this.phoneNumberFormArray.controls.filter((group) => group.get('isMain').value).length;
      if (isMainPhoneNumberCount === 0) {
        this.phoneNumberFormArray.controls[0].get('isMain').setValue(true);
      }
    }
  }

  resetOtherIsMain(index: number) {
    this.phoneNumberFormArray = this.contactForm.get('phoneNumbers') as FormArray;
    this.phoneNumberFormArray.controls.forEach((group, i) => {
      if (i !== index) {
        group.get('isMain').setValue(false);
      }
    });
  }

  setFields() {
      this.contactForm.controls.firstName.setValue(this.editableContact.firstName);
      this.contactForm.controls.lastName.setValue(this.editableContact.lastName);
      this.contactForm.controls.email.setValue(this.editableContact.email);
      this.contactForm.controls.birthday.setValue(this.editableContact.birthday);
      const birthdayDatepicker = $('#birthdayInput');
      birthdayDatepicker.datepicker('update', this.editableContact.birthday);
      this.editableContact.phoneNumbers.forEach(phoneNumber => {
        this.addPhoneNumberGroup(phoneNumber);
      });
  }

  getEditableContact(id: string) {
    return this.contactService.getContactById(id).get().then(res => {
      this.editableContact = res.data() as Contact;
      this.setFields();
    });
  }

  createContact() {
    const phoneNumbers: PhoneNumber[] =  [];
    this.phoneNumberFormArray.controls.forEach((group) => {
      if (group.get('phoneNumber').value.length > 0) {
        const phoneNumber: PhoneNumber = {
          phoneNumber: group.get('phoneNumber').value,
          isMain: group.get('isMain').value,
          category: group.get('category').value,
        };
        phoneNumbers.push(phoneNumber);
      }
    });
    const contact: Contact = {
      id: null,
      firstName: this.contactForm.controls.firstName.value,
      lastName: this.contactForm.controls.lastName.value,
      phoneNumbers: [...phoneNumbers],
      email: this.contactForm.controls.email.value || null,
      birthday: this.contactForm.controls.birthday.value || null,
      avatarUrl: null
    };
    if (this.editMode) {
      this.updateContact(this.editableContactId, contact);
    } else {
      this.saveContact(contact);
    }
  }

  saveContact(contact: Contact) {
    this.contactService.createContact(contact)
      .then((docRef) => {
      console.log('Document written with ID: ', docRef.id);
      this.router.navigate(['/']);
    })
      .catch((error) => {
        console.error('Error adding document: ', error);
      });
  }

  updateContact(id: string, contact: Contact) {
    this.contactService.updateContactById(id, contact);
    this.router.navigate(['/']);
  }

  removeContact() {
    this.contactService.removeContactById(this.editableContactId)
      .then(() => {
        this.router.navigate(['/']);
      })
      .catch((error) => {
        console.error('Error removing document: ', error);
      });
  }

  displayFieldCss(form: FormGroup, field: string) {
    return {
      'has-error': this.isInvalid(form, field),
      'has-feedback': this.isInvalid(form, field),
      'has-success': !this.isInvalid(form, field)
    };
  }

  isInvalid(form: FormGroup, field: string) {
    return !form.get(field).valid && form.get(field).touched;
  }

}
