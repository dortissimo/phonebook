import {Contact} from './contact';

export interface PhoneBook {
  name: string;
  contacts: Contact[];
}
