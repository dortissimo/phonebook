import {PhoneNumberCategory} from './enum/phoneNumberCategory';
export interface PhoneNumber {
  phoneNumber: string;
  isMain: boolean;
  category: PhoneNumberCategory;
}
