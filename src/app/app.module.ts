import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './smart/navbar/navbar.component';
import { ContentComponent } from './dump/content/content.component';
import { ContactSearchComponent } from './dump/contact-search/contact-search.component';
import { ContactListComponent } from './dump/contact-list/contact-list.component';
import { ContactOptionComponent } from './dump/contact-option/contact-option.component';
import { AllContactsComponent } from './smart/all-contacts/all-contacts.component';
import { CreateEditContactComponent } from './smart/create-edit-contact/create-edit-contact.component';
import {ReactiveFormsModule} from '@angular/forms';
import { MainPhoneNumberPipe } from './pipe/main-phone-number.pipe';
import { FullNamePipe } from './pipe/full-name.pipe';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ContentComponent,
    ContactSearchComponent,
    ContactListComponent,
    ContactOptionComponent,
    AllContactsComponent,
    CreateEditContactComponent,
    MainPhoneNumberPipe,
    FullNamePipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
